package com.example.users;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.users.exceptions.EmailException;
import com.example.users.exceptions.PasswordException;
import com.example.users.models.User;
import com.example.users.repository.UserRepository;
import com.example.users.services.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
@RunWith(SpringJUnit4ClassRunner.class)
class UsersApplicationTest {
	@Mock
	private UserRepository repository;

	@InjectMocks
	private UserServiceImpl service;

	@Test
	public void userEmailIsNotValid() {
		User request = new User();
		request.setEmail("test.com");

		assertEquals(false, service.validEmail(request.getEmail()));

	}

	@Test
	public void userPasswordIsNotValid() {
		User request = new User();
		request.setEmail("j@test.com");
		request.setPassword("hunter2");

		assertEquals(false, service.validPassword(request.getPassword()));

	}

	@Test
	public void userEmailIsNotValidException() {

		User request = new User();
		request.setEmail("test.com");

		try {
			service.create(request);
		} catch (EmailException e) {
			assertEquals(null, e.getMessage());
		}
	}

	@Test
	public void userPasswordIsNotValidException() {

		User request = new User();
		request.setEmail("tj@est.com");
		request.setPassword("hunter2");

		try {
			service.create(request);
		} catch (PasswordException e) {
			assertEquals(null, e.getMessage());
		}
	}

}
