package com.example.users;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.Assert;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc(print = MockMvcPrint.SYSTEM_ERR)
@TestMethodOrder(OrderAnnotation.class)
@TestInstance(Lifecycle.PER_CLASS)
public class UserServiceTest {

	/** The mvc. */
	@Autowired
	protected MockMvc mvc;

	/** The object mapper. */
	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void userServiceTest() {
		String productOffering = "{ \"name\": \"Juan Rodriguez\", \"email\": \"j@gmail.com\", \"password\": \"Hunter24\", \"phones\":[ { \"number\": \"1234567\", \"citycode\": \"1\", \"contrycode\": \"57\" } ] }";
		MvcResult andReturn;

		try {
			andReturn = mvc
					.perform(post("/user").content(productOffering).contentType(MediaType.APPLICATION_JSON_VALUE)
							.accept(MediaType.APPLICATION_JSON).characterEncoding("UTF-8"))
					.andExpect(status().isCreated()).andReturn();
			Assert.notNull(andReturn.getResponse().getContentAsString(), "Response is empty");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
