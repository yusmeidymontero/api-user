package com.example.users.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.users.dao.UserMapper;
import com.example.users.dao.UserMapperImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class UserAuthenticationConfig {

	@Bean
	public IAuthentication iAuthentication(@Value("${jwt.secret}") String secret) {
		return new JWTAuthorizationFilter(secret);
	}

	@Bean
	public UserMapper userMapper(ObjectMapper objectMapper) {
		return new UserMapperImpl(objectMapper);
	}
}
