package com.example.users.config;

import java.security.Key;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

public class JWTAuthorizationFilter implements IAuthentication {

	private final String secret;

	public JWTAuthorizationFilter(String secret) {
		this.secret = secret;
	}

	@Override
	public String authorize(String email) {
		Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
		return Jwts.builder().setSubject(email).signWith(key).compact();
	}
}
