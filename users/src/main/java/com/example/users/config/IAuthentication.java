package com.example.users.config;

public interface IAuthentication {
	String authorize(String email);
}
