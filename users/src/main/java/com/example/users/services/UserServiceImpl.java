package com.example.users.services;

import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.users.config.IAuthentication;
import com.example.users.dao.IUser;
import com.example.users.dao.UserMapper;
import com.example.users.exceptions.EmailException;
import com.example.users.exceptions.PasswordException;
import com.example.users.exceptions.UserNotFoundException;
import com.example.users.models.User;
import com.example.users.models.UserEntity;
import com.example.users.repository.UserRepository;

@Service
public class UserServiceImpl implements IUser {
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository repository;

	private final IAuthentication authentication;
	private final UserMapper userMapper;

	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern
			.compile("^([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$");

	public static final Pattern VALID_PASSWORD_REGEX = Pattern.compile(
			"(^[A-Z][a-z]+(\\d){2}$)|(^[a-z]+[A-Z](\\d){2}$)|(^(\\d){2}[A-Z]{1}[a-z]+$)|(^[a-z]+(\\d){2}[A-Z])");

	public UserServiceImpl(IAuthentication authentication, UserMapper userMapper) {
		this.authentication = authentication;
		this.userMapper = userMapper;
	}

	@Override
	public User authenticate(String email, String password) {
		logger.info("email: {}", email);
		logger.info("password: {}", password);

		UserEntity entity = repository.findByEmail(email);

		if (Objects.isNull(entity))
			throw new UserNotFoundException();

		if (!entity.getPassword().equals(password))
			throw new PasswordException();

		return userMapper.toModel(entity);
	}

	@Override
	public boolean validEmail(String email) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		return matcher.matches();
	}

	@Override
	public boolean validPassword(String password) {
		Matcher matcher = VALID_PASSWORD_REGEX.matcher(password);
		return matcher.matches();
	}

	@Override
	public User create(User user) {
		if (!validEmail(user.getEmail()))
			throw new EmailException();

		if (!validPassword(user.getPassword()))
			throw new PasswordException();

		UserEntity userEntity = repository.findByEmail(user.getEmail());

		if (Objects.nonNull(userEntity))
			return null;

		Optional.ofNullable(user.getEmail()).map(authentication::authorize).ifPresent(user::setToken);

		return userMapper.toModel(repository.save(userMapper.toEntity(user)));
	}
}
