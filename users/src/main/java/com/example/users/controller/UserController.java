package com.example.users.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.users.exceptions.UserExistException;
import com.example.users.models.User;
import com.example.users.services.UserServiceImpl;

@RestController
@RequestMapping(value = "/", produces = { "application/json; charset=UTF-8" })
public class UserController {

	@Autowired
	private UserServiceImpl service;

	@PostMapping("user")
	public ResponseEntity<User> userCreate(@RequestBody User request) {

		User response = service.create(request);

		if (Objects.isNull(response))
			throw new UserExistException();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping("login")
	public ResponseEntity<User> login(@RequestParam(name = "email", required = true) String email,
			@RequestParam(name = "password", required = true) String password) {
		User response = service.authenticate(email, password);

		if (Objects.isNull(response))
			throw new UserExistException();

		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
