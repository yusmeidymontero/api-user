package com.example.users.dao;

import com.example.users.models.User;
import com.example.users.models.UserEntity;

public interface UserMapper {
	UserEntity toEntity(User user);

	User toModel(UserEntity userEntity);
}
