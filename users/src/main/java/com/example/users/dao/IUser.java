package com.example.users.dao;

import com.example.users.models.User;

public interface IUser {
	User authenticate(String email, String password);

	boolean validEmail(String email);

	boolean validPassword(String password);

	User create(User user);
}
