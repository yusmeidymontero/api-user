package com.example.users.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.users.models.User;
import com.example.users.models.UserEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UserMapperImpl implements UserMapper {
	private static final Logger logger = LoggerFactory.getLogger(UserMapperImpl.class);

	private ObjectMapper objectMapper;

	public UserMapperImpl(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Override
	public UserEntity toEntity(User user) {

		try {
			return UserEntity.builder().name(user.getName()).email(user.getEmail()).password(user.getPassword())
					.phones(objectMapper.writeValueAsString(user.getPhones()))
					.created(Optional.ofNullable(user.getCreated()).orElse(new Timestamp(System.currentTimeMillis())))
					.modified(Optional.ofNullable(user.getModified()).orElse(new Timestamp(System.currentTimeMillis())))
					.lastLogin(
							Optional.ofNullable(user.getLastLogin()).orElse(new Timestamp(System.currentTimeMillis())))
					.isActive(true).token(user.getToken()).build();
		} catch (JsonProcessingException e) {
			logger.error("Error", e.getCause());
		}

		return null;
	}

	@Override
	public User toModel(UserEntity userEntity) {
		try {
			return User.builder().id(userEntity.getId()).name(userEntity.getName()).email(userEntity.getEmail())
					.password(userEntity.getPassword())
					.phones(objectMapper.readValue(userEntity.getPhones(), ArrayList.class))
					.created(Optional.ofNullable(userEntity.getCreated())
							.orElse(new Timestamp(System.currentTimeMillis())))
					.modified(Optional.ofNullable(userEntity.getModified())
							.orElse(new Timestamp(System.currentTimeMillis())))
					.lastLogin(Optional.ofNullable(userEntity.getLastLogin())
							.orElse(new Timestamp(System.currentTimeMillis())))
					.token(userEntity.getToken()).isActive(true).build();
		} catch (JsonProcessingException e) {
			logger.error("Error", e.getCause());
		}

		return null;
	}

}
