package com.example.users.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.users.models.UserEntity;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, UUID> {
	UserEntity findByEmail(String email);
}
