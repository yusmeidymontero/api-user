package com.example.users.exceptions;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(EmailException.class)
	public ResponseEntity<ApiError> emailExceptionHandle(EmailException ex) {
		return new ResponseEntity<>(new ApiError("Formato de email incorrecto"), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(PasswordException.class)
	public ResponseEntity<ApiError> passwordExceptionHandle(PasswordException ex) {
		return new ResponseEntity<>(new ApiError("Formato de contraseņa incorrecto"), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(UserException.class)
	public ResponseEntity<ApiError> userExceptionHandle(UserException ex) {
		return new ResponseEntity<>(new ApiError("Error user null"), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<ApiError> userNotFoundExceptionHandle(UserNotFoundException ex) {
		return new ResponseEntity<>(new ApiError("Error usuario no encontrado"), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(UserExistException.class)
	public ResponseEntity<ApiError> userExistExceptionHandle(UserExistException ex) {
		return new ResponseEntity<>(new ApiError("Usuario ya registrado"), HttpStatus.BAD_REQUEST);
	}
}
